var connection = require('../connection');

function Todo() {
  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from todo_list', function(err, result) {
        con.release();
        res.send(result);
      });
    });
  };

  this.create = function(todo, res) {
    connection.acquire(function(err, con) {
      con.query('insert into todo_list set description ='+ todo, function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'TODO creation failed'});
        } else {
          res.send({status: 0, message: 'TODO created successfully'});
        }
      });
    });
  };

  this.update = function(todo, res) {
    connection.acquire(function(err, con) {
      con.query('update todo_list set ? where id = ?', [todo, todo.id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'TODO update failed'});
        } else {
          res.send({status: 0, message: 'TODO updated successfully'});
        }
      });
    });
  };

  this.delete = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('delete from todo_list where id = ?', [id], function(err, result) {
        con.release();
        if (err) {
          res.send({status: 1, message: 'Failed to delete'});
        } else {
          res.send({status: 0, message: 'Deleted successfully'});
        }
      });
    });
  };

//this is my answers for the question 5

  this.put = function(id,res){
    connection.acquire(function(err,con)){
      con.query('update todo_list set done where id = ?', [todo, todo.id], function(err,result)){
        con.release();
        if (err) {
          res.send({status: 1 message: 'TODO update failed'});
        }
        else {
          res.send({status: 0 message: 'TODO update failed'});
        };
      };
    };
  };

  //this is my answer for the question 6

  this.get = function(id,res){
    connection.acquire(function(err,con)){
      con.query('select * from todo_list where status = 0 order by rand() limit 0,10', [todo, todo.id], function(err,result)){
        con.release();
        if (err) {
          res.send({status: 1 message: 'TODO get failed'});
        }
        else {
          res.send({status:0 message: 'TODO get successfully'});
        };
      };
    };
  };
}

module.exports = new Todo();
